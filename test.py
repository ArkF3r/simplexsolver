from solver import solverSimplex as sS
import json

if __name__ == '__main__':
    # data = {
    #     'objetivo': 'Minimizar',#'Maximizar',
    #     'noVariables': 2,
    #     'funcionObjetivo': [ 1, 1 ],
    #     'restricciones': [ [1, 2, 1, 3], [0, 1, 2, 10] ]
    # }
    # data = {
    #     'objetivo': 'Minimizar',#'Maximizar',
    #     'noVariables': 2,
    #     'funcionObjetivo': [ 1, 3 ],
    #     'restricciones': [ [1, 2, 1, 10], [1, 1, 2, 15] ]
    # }
    # data = {
    #     'objetivo': 'Maximizar',
    #     'noVariables': 2,
    #     'funcionObjetivo': [ 2, 2 ],
    #     'restricciones': [ [0, 1, 2, 50], [1, 1, 1, 10], [0, 3, 1, 60] ]
    # }
    # data = {
    #     'objetivo': 'Minimizar',
    #     'noVariables': 3,
    #     'funcionObjetivo': [ .8, .6, .35 ],
    #     'restricciones': [ [1, 1, 0, 0, 1250], [1, 0, 1, 0, 750], [1, .45, .45, .1, 1000], [1, .3, .3, .4, 500], [1, .2, .2, .6, 1500], 
    #     [0, 1, 0, 0, 3000], [0, 0, 1, 0, 2000] ]
    # }
    # data = {
    #     'objetivo': 'Maximizar',
    #     'noVariables': 7,
    #     'funcionObjetivo': [ 1250, 750, 1000, 500, 1500, -3000, -2000 ],
    #     'restricciones': [ [ 0, 1, 0, .45, .3, .2, -1, 0, .8 ], [ 0, 0, 1, .45, .3, .2, 0, -1, .6 ], [ 0, 0, 0, .1, .4, .6, 0, 0, .35 ] ]
    # }
    # data = '{"objetivo":"Maximizar","noVariables":2,"funcionObjetivo":[1,1],"restricciones":[[0,2,1,20],[1,1,1,10]]}'
    # data = '{"objetivo":"Minimizar","noVariables":2,"funcionObjetivo":[1,1],"restricciones":[[0,2,1,20],[1,1,1,10]]}'
    # data = '{"objetivo":"Maximizar","noVariables":3,"funcionObjetivo":[1,1,1],"restricciones":[[0,1,0,1,50],[0,2,1,0,75],[1,1,0,-1,10],[1,0,0,1,-5]]}'
    # data = '{"objetivo":"Minimizar","noVariables":3,"funcionObjetivo":[1,1,1],"restricciones":[[0,1,0,1,50],[0,2,1,0,75],[1,1,0,-1,10],[1,0,0,1,-5]]}'
    data = '{"objetivo":"Maximizar","noVariables":4,"funcionObjetivo":[1,-1,1,1],"restricciones":[[0,1,1,0,0,30],[0,1,0,0,1,40],[2,1,1,1,1,100]]}'
    # data = '{"objetivo":"Minimizar","noVariables":4,"funcionObjetivo":[1,-1,1,1],"restricciones":[[0,1,1,0,0,30],[0,1,0,0,1,40],[2,1,1,1,1,100]]}'
    # data = '{"objetivo":"Maximizar","noVariables":4,"funcionObjetivo":[1,1,-2,1],"restricciones":[[0,1,0,1,0,50],[0,0,1,0,1,75],[1,1,0,0,0,10],[0,0,1,0,1,100],[1,0,0,2,1,30]]}'
    # data = '{"objetivo":"Minimizar","noVariables":4,"funcionObjetivo":[1,1,-2,1],"restricciones":[[0,1,0,1,0,50],[0,0,1,0,1,75],[1,1,0,0,0,10],[0,0,1,0,1,100],[1,0,0,2,1,30]]}'
    data = json.loads(data)
    print(data)
    solSimplex = sS(data)
    solSimplex.solveProblem()
    solSimplex.printMatrices()
    solSimplex.getSolution()
    print('Indeterminado: {}'.format(solSimplex.Indeterminado)) 
    print('Resuelto: {}'.format(solSimplex.status))
